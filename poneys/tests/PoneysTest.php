<?php
  require_once 'src/Poneys.php';

  class PoneysTest extends \PHPUnit_Framework_TestCase {

    private $poneys;

    public function setUp(){
      $this->poneys = new Poneys();
      if(isset($_ENV['TAILLE'])) $this->poneys->setTaille(intval($_ENV['TAILLE']));
      if(isset($_ENV['poneys'])) $this->poneys->setCount(intval($_ENV['poneys']));
    }

    public function tearDown(){
      unset($this->poneys);
    }
    
    /**
    * @dataProvider negativeProvider
    */
    public function test_removePoneyFromField($n, $r) {
      // Action
      $this->poneys->removePoneyFromField($n);
      
      // Assert
      $this->assertEquals($r, $this->poneys->getCount());
    }

    public function test_getNames(){
      $mock = $this->getMockBuilder(Poneys::class)
                   ->setMethods(['getNames'])
                   ->getMock();

      $mock->expects($this->exactly(1))->method('getNames')->willReturn([ "Poney1", "Poney2", "Poney3" ]);

      $this->assertEquals([ "Poney1", "Poney2", "Poney3" ], $mock->getNames());

    }

    /**
    * @dataProvider hasAvailableProvider
    */
    public function test_hasAvailable($a, $result){
      $this->poneys->setCount($a);
      $this->assertEquals($result, $this->poneys->hasAvailable());
    }

    public function negativeProvider()
    {
        return [
            [0, 8],
            [1, 7]
        ];
    }

    public function hasAvailableProvider(){
      return [
        [0, true],
        [1, true],
        [2, true],
        [3, true],
        [4, true],
        [5, true],
        [6, true],
        [7, true],
        [8, true],
        [9, true],
        [10, true],
        [11, true],
        [12, true],
        [13, true],
        [14, true],
        [15, false]
      ];
    }
  }
 ?>
