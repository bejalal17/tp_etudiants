<?php 
  class Poneys {
      private $count = 8;

      private $taille = 15;

      public function getCount() {
        return $this->count;
      }

      public function removePoneyFromField($number) {
        if($number >= 0){
          $this->count -= $number;  
        } else {
          throw new Exception("Nombre négatif");
        }
        
      }

      public function getTaille(){
        return $this->taille;
      }

      public function setTaille($n){
        $this->taille = $n;
      }

      public function hasAvailable(){
        return $this->count < $this->getTaille() && $this->count >= 0 ;
      }

      public function addPoneyFromField($number){
        $this->count += $number;
      }

      public function getNames() {

      }

      public function setCount($n){
        $this->count = $n;
      }
  }
?>
